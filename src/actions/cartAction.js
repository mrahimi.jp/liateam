/* eslint-disable no-param-reassign */
/* eslint-disable no-plusplus */
import { ADD_TO_CART, REMOVE_FROM_CART } from '../constatnts/action-types';

export const addToCart = (product) => (dispatch, getState) => {
  const cartItems = getState().cartItems.slice();
  let alreadyExists = false;
  cartItems.forEach((x) => {
    if (x.id === product.id) {
      alreadyExists = true;
      x.count++;
    }
  });
  if (!alreadyExists) {
    cartItems.push({ ...product, count: 1 });
  }
  dispatch({
    type: ADD_TO_CART,
    payload: { cartItems }
  });
  localStorage.setItem('cartItems', JSON.stringify(cartItems));
};

export const countUp = (product) => (dispatch, getState) => {
  const cartItems = getState().cartItems.slice();
  cartItems.forEach((x) => {
    if (x.id === product.id) {
      x.count++;
    }
  });
  dispatch({
    type: ADD_TO_CART,
    payload: { cartItems }
  });
  localStorage.setItem('cartItems', JSON.stringify(cartItems));
};

export const countDown = (product) => (dispatch, getState) => {
  const cartItems = getState()
    .cartItems.slice()
    .filter((x) => {
      if (x.id === product.id) {
        x.count--;
      }
      return x.count > 0;
    });
  dispatch({
    type: ADD_TO_CART,
    payload: { cartItems }
  });
  localStorage.setItem('cartItems', JSON.stringify(cartItems));
};

export const removeFromCart = (product) => (dispatch, getState) => {
  const cartItems = getState()
    .cartItems.slice()
    .filter((x) => x.id !== product.id);
  dispatch({ type: REMOVE_FROM_CART, payload: { cartItems } });
  localStorage.setItem('cartItems', JSON.stringify(cartItems));
};
