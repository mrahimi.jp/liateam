import React from 'react';
import { Link } from 'react-router-dom';
import { Box } from '@material-ui/core';
import useStyles from './item.styles';
import CategoryCart from '../../assets/images/category_cart.png';

export default function Item() {
  const classes = useStyles();

  return (
    <Box className={classes.root} component={Link} to="/products">
      <img className={classes.image} src={CategoryCart} alt="liateam" />
    </Box>
  );
}
