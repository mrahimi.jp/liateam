import { Container, Grid, Typography } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import useStyles from './index.styles';
import Item from './Item';

export default function Index() {
  const classes = useStyles();
  const [data, setData] = useState();

  useEffect(() => {
    axios({
      url: 'https://shop.rc.liadev.ir/api/rest/v1/get_categories',
      method: 'GET'
    }).then((response) => {
      setData(response.data);
      console.log(response);
    });
  }, []);

  return (
    <Container maxWidth="lg" className={classes.root}>
      <Typography className={classes.title} variant="h2">
        دسته بندی
      </Typography>
      <Grid className={classes.categoryCon} container>
        <Item />
        <Item />
        <Item />
        <Item />
        <Item />
        <Item />
      </Grid>
    </Container>
  );
}
