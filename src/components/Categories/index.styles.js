import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  categoryCon: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    marginTop: 50,
    marginLeft: 60
  }
}));
