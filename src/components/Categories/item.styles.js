import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  root: {
    marginTop: 51,
    marginBottom: 51,
    marginLeft: 65,
    marginRight: 65
  },
  image: {
    width: 500,
    height: '100%',
    cursor: 'pointer',
    transition: '.5s',
    '&:hover': {
      transform: 'translateY(-10px)',
      boxShadow: '2px 8px 15px 1px #3c5b68',
      transition: '.5s'
    }
  }
}));
