/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useState, useEffect, useRef } from 'react';
import { Button, Fade, Typography } from '@material-ui/core';
import { connect } from 'react-redux';
import useStyles from './cartButton.styles';
import CartIcon from '../../../assets/images/cart.png';
import CartItem from './CartItem';
import useOutsideClick from '../../../Hook/useOutsideClick';

function CartButton(props) {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [count, setCount] = useState(0);
  const [price, setPrice] = useState(0);

  useEffect(() => {
    let cartCount = 0;
    props.cartItems.forEach((item) => {
      cartCount += item.count;
    });
    setCount(cartCount);
    let cartPrice = 0;
    props.cartItems.forEach((item) => {
      cartPrice += item.price;
    });
    setPrice(cartPrice);
  }, [props.cartItems]);

  const cart = useRef();
  useOutsideClick(cart, () => setOpen(false));

  return (
    <div className={classes.root} ref={cart}>
      <div
        className={classes.topDiv}
        onClick={() => {
          setOpen(!open);
        }}
      >
        <img src={CartIcon} alt="" className={classes.shopIcon} />
        {count !== 0 ? (
          <div className={classes.badge}>
            <Typography variant="subtitle2" color="textPrimary">
              {count}
            </Typography>
          </div>
        ) : null}
      </div>
      <Fade in={open}>
        <div>
          <span className={classes.triangle} />
          <div className={classes.cartContent}>
            {props.cartItems.map((item, i) => (
              <CartItem key={item.id} product={item} index={i} />
            ))}
            <div className={classes.priceDiv}>
              <Typography className={classes.priceTitle} variant="h5">
                : جمع کل
              </Typography>
              <Typography className={classes.price} variant="h6">
                {price} تومان
              </Typography>
              <Typography className={classes.priceTitle} variant="h5">
                : مبلغ قابل پرداخت
              </Typography>
              <Typography className={classes.price} variant="h6">
                {price} تومان
              </Typography>
            </div>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
            >
              ثبت سفارش
            </Button>
          </div>
        </div>
      </Fade>
    </div>
  );
}

export default connect((state) => ({
  cartItems: state.cartItems
}))(CartButton);
