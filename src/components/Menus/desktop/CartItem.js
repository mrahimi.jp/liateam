/* eslint-disable react/destructuring-assignment */
import { Box, Typography } from '@material-ui/core';
import React, { useState, useEffect } from 'react';
import DeleteIcon from '@material-ui/icons/Close';
import AddIcon from '@material-ui/icons/Add';
import MinusIcon from '@material-ui/icons/Minimize';
import { connect } from 'react-redux';
import useStyles from './cartItem.styles';
import {
  removeFromCart,
  countUp,
  countDown
} from '../../../actions/cartAction';

function CartItem(props) {
  const classes = useStyles();
  const bg = props.index % 2 !== 0 ? '#3D3D3F' : 'transparent';
  const [counter, setCounter] = useState(props.product.count);

  useEffect(() => {
    setCounter(props.product.count);
  }, [props.cartItems, props.product.count]);

  return (
    <div className={classes.root} style={{ backgroundColor: bg }}>
      <div className={classes.rightContent}>
        <img src={props.product.image} alt="" className={classes.image} />
        <div className={classes.descDiv}>
          <Typography className={classes.title} variant="subtitle2">
            {props.product.title}
          </Typography>
          <Typography className={classes.price} variant="subtitle2">
            {props.product.price} تومان
          </Typography>
          <div className={classes.counterDiv}>
            <Box className={classes.counterBox}>
              <AddIcon
                className={classes.counterIcon}
                onClick={() => {
                  props.countUp(props.product);
                }}
              />
              <Typography className={classes.counterText}>{counter}</Typography>
              <MinusIcon
                className={classes.counterIcon}
                onClick={() => {
                  props.countDown(props.product);
                }}
                style={{ paddingBottom: 8 }}
              />
            </Box>
          </div>
        </div>
      </div>
      <Box
        onClick={() => {
          props.removeFromCart(props.product);
        }}
        className={classes.deleteDiv}
      >
        <DeleteIcon className={classes.deleteIcon} />
      </Box>
    </div>
  );
}

export default connect(
  (state) => ({
    cartItems: state.cartItems
  }),
  { removeFromCart, countDown, countUp }
)(CartItem);
