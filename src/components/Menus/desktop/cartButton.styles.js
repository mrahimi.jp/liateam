import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  root: {
    height: '100%',
    width: 80,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingRight: 5,
    paddingLeft: 5,
    // border: '1px #202d2a solid',
    position: 'relative'
  },
  topDiv: {
    position: 'relative',
    transition: '.5s',
    cursor: 'pointer',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    '&:hover': {
      opacity: 0.5,
      transition: '.5s'
    },
    [theme.breakpoints.down('xs')]: {
      '&:hover': {
        opacity: 1,
        transition: '.5s'
      }
    }
  },
  shopIcon: {
    width: 35
  },
  badge: {
    width: 22,
    height: 22,
    backgroundColor: 'red',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 100,
    position: 'absolute',
    top: 0,
    left: -5
  },
  triangle: {
    width: 20,
    height: 20,
    backgroundColor: '#202124',
    transform: 'rotate(45deg)',
    position: 'absolute',
    bottom: '-5px',
    right: 32
  },
  cartContent: {
    width: 331,
    backgroundColor: '#202124',
    position: 'absolute',
    top: 72,
    right: 5,
    zIndex: 1,
    maxHeight: 490,
    overflowX: 'hidden',
    overflowY: 'scroll'
  },
  priceDiv: {
    padding: 30,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap',
    backgroundColor: '#3D3D3F'
  },
  priceTitle: {
    width: '50%',
    textAlign: 'left',
    direction: 'rtl',
    color: '#E5E5E5',
    lineHeight: 2
  },
  price: {
    width: '50%',
    textAlign: 'right',
    color: '#B5B5B5',
    lineHeight: 2
  },
  button: {
    width: 257,
    height: 60,
    borderRadius: 8,
    margin: 35
  }
}));
