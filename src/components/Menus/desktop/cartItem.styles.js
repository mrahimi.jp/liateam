import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  root: {
    padding: 20,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    boxShadow: '0px 4px 6px 1px #3a3939'
  },
  deleteIcon: {
    width: 22,
    color: '#D4D3D3',
    cursor: 'pointer'
  },
  image: {
    height: 95,
    padding: 5
  },
  title: {
    color: '#FFF',
    width: '100%',
    textAlign: 'left',
    paddingLeft: 5,
    paddingBottom: 2
  },
  price: {
    color: '#B5B5B5',
    width: '100%',
    textAlign: 'left'
  },
  rightContent: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%'
  },
  descDiv: {
    marginLeft: 10,
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'top'
  },
  deleteDiv: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  counterDiv: {
    height: '50%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  counterBox: {
    width: 86,
    height: 30,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    border: '1px solid #3D3D3F',
    borderRadius: 5,
    marginTop: 10
  },
  counterIcon: {
    color: '#FFF',
    fontSize: 20,
    cursor: 'pointer',
    paddingRight: 2,
    paddingLeft: 2
  },
  counterText: {
    fontSize: 12,
    color: '#FFF'
  }
}));
