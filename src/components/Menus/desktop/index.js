import React from 'react';
import { Button, Container, Typography } from '@material-ui/core';
import { Link, useLocation } from 'react-router-dom';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import useStyles from './index.styles';
import brandLogo from '../../../assets/images/logo.png';
import avatarIcon from '../../../assets/images/avatar.png';
import CartButton from './CartButton';

export default function Desktop() {
  const classes = useStyles();
  const location = useLocation();
  const path = location.pathname;

  return (
    <div className={classes.root}>
      <Container maxWidth="lg" className={classes.topMenuCon}>
        <img src={brandLogo} alt="liateam" className={classes.brandImg} />
        <div className={classes.topMenu}>
          <Button className={classes.topMenuItem} style={{ color: '#202124' }}>
            خانه
          </Button>
          <Button className={classes.topMenuItem}>فروشگاه</Button>
          <Button className={classes.topMenuItem}>وبلاگ</Button>
          <Button className={classes.topMenuItem}>درباره ما</Button>
          <Button className={classes.topMenuItem}>تماس با ما</Button>
        </div>
        <Typography variant="body1">رضا پور جباری عزیز</Typography>
        <img className={classes.avatar} src={avatarIcon} alt="user" />
        <ExpandMoreIcon />
      </Container>
      <Container maxWidth="lg" className={classes.bottomMenuCon}>
        <div className={classes.bottomMenu}>
          <div className={classes.activeMenuDiv}>
            <Typography className={classes.bottomMenuItem}>
              مراقبت پوست
            </Typography>
          </div>
          <div className={classes.inactiveMenuDiv}>
            <Typography className={classes.bottomMenuItem}>
              مراقبت مو
            </Typography>
          </div>
          <div className={classes.inactiveMenuDiv}>
            <Typography className={classes.bottomMenuItem}>
              مراقبت بدن
            </Typography>
          </div>
          <div className={classes.inactiveMenuDiv}>
            <Typography className={classes.bottomMenuItem}>آرایشی</Typography>
          </div>
          <div className={classes.inactiveMenuDiv}>
            <Typography className={classes.bottomMenuItem}>
              پرفروشترین
            </Typography>
          </div>
          <div className={classes.inactiveMenuDiv}>
            <Typography className={classes.bottomMenuItem}>جدیدترین</Typography>
          </div>
        </div>
        <div className={classes.leftMenuDiv}>
          <CartButton />
        </div>
      </Container>
    </div>
  );
}
