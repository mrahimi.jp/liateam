import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  root: {
    width: '100%',
    height: 192,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: '#FFF',
    boxShadow: '1px -3px 20px 0px #3b3939'
  },
  brandImg: {
    width: 85,
    height: 106,
    marginTop: 5,
    marginLeft: 50
  },
  topMenuCon: {
    height: 115,
    display: 'flex',
    alignItems: 'center',
    borderBottom: '3px solid #EDEDED'
  },
  topMenu: {
    flex: 1,
    height: '100%',
    display: 'flex',
    alignItems: 'center'
  },
  topMenuItem: {
    color: '#E0E0E0',
    fontSize: 12
  },
  avatar: {
    width: 50,
    height: 50,
    marginLeft: 50
  },
  bottomMenuCon: {
    height: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  bottomMenu: {
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },
  bottomMenuItem: {
    color: '#202124',
    fontSize: 12,
    textAlign: 'left',
    width: 104,
    height: 30,
    cursor: 'pointer'
  },
  activeMenuDiv: {
    borderBottomColor: '#EC407A',
    borderBottomWidth: 6,
    borderBottomStyle: 'solid',
    marginRight: 30,
    margingLeft: 30
  },
  inactiveMenuDiv: {
    marginRight: 30,
    margingLeft: 30
  },
  leftMenuDiv: {
    height: '100%',
    // paddingRight: 40,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  }
}));
