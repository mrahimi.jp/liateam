/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { Typography } from '@material-ui/core';
import { connect } from 'react-redux';
import useStyles from './item.styles';
import CartPlusIcon from '../../assets/images/cart-plus.png';
import { addToCart } from '../../actions/cartAction';

function Item(props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.topDiv}>
        <img
          className={classes.image}
          src={props.product.image}
          alt="liateam"
        />
        {props.product.new ? (
          <div className={classes.statusDiv}>
            <Typography variant="subtitle2" color="textSecondary">
              جدید
            </Typography>
          </div>
        ) : null}
      </div>
      <Typography className={classes.title} variant="body2" color="textPrimary">
        {props.product.title} {props.product.id}
      </Typography>
      <Typography
        className={classes.desc}
        variant="subtitle2"
        color="textSecondary"
      >
        {props.product.desc}
      </Typography>
      <div className={classes.bottomDiv}>
        <img
          className={classes.cartPlus}
          onClick={() => {
            props.addToCart(props.product);
          }}
          src={CartPlusIcon}
          alt="liateam"
        />
        <div className={classes.priceDiv}>
          <Typography className={classes.price} variant="h3">
            {props.product.price}
          </Typography>
          <Typography variant="subtitle1">تومان</Typography>
        </div>
      </div>
    </div>
  );
}

export default connect(null, { addToCart })(Item);
