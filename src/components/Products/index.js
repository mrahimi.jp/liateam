/* eslint-disable react-hooks/exhaustive-deps */
import { Container, Grid, Typography } from '@material-ui/core';
import Pagination from '@material-ui/lab/Pagination';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import useStyles from './index.styles';
import Item from './Item';
import productImage from '../../assets/images/product.png';

const products = [
  {
    id: 1,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 2,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: false,
    image: productImage
  },
  {
    id: 3,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 4,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 5,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 6,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 7,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 8,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 9,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 10,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 11,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 12,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 13,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 14,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 15,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 16,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 17,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 18,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 19,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 20,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 21,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 22,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 23,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 24,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 25,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 26,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 27,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 28,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 29,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 30,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 31,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 32,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 33,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 34,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  },
  {
    id: 35,
    title: 'شامپو سر عصاره خاویار',
    desc: '400ml',
    price: 3000000,
    new: true,
    image: productImage
  }
];

export default function Index() {
  const classes = useStyles();
  const pageSize = 12;
  const [data, setData] = useState(products);
  const [currentData, setCurrentData] = useState(data.slice(0, pageSize));
  const [page, setPage] = useState(1);
  const [count, setCount] = useState(0);

  useEffect(() => {
    // axios({
    //   url: 'https://shop.rc.liadev.ir/api/rest/v1/get_product?categories=10',
    //   method: 'GET'
    // }).then((response) => {
    //   setData(response.data);
    // });
    // setCurrentData(data.slice(0, pageSize - 1));
    setCount(Math.trunc(products.length / pageSize) + 1);
  }, []);

  useEffect(() => {
    const endIndex = page * pageSize;
    const startIndex = endIndex - pageSize;
    setCurrentData(data.slice(startIndex, endIndex));
  }, [page]);

  return (
    <Container maxWidth="lg" className={classes.root}>
      <Typography className={classes.title} variant="h2">
        محصولات
      </Typography>
      <Grid className={classes.categoryCon} container>
        {currentData.map((product) => (
          <Item key={product.id} product={product} />
        ))}
      </Grid>
      <Pagination
        className={classes.pagination}
        count={count}
        page={page}
        color="primary"
        onChange={(event, val) => setPage(val)}
      />
    </Container>
  );
}
