import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  root: {
    width: 263,
    height: 370,
    marginRight: 29,
    marginTop: 30,
    cursor: 'pointer',
    transition: '.5s',
    boxShadow: '0px 0px 4px 0px #d4e2e8',
    '&:hover': {
      transform: 'translateY(-2px)',
      boxShadow: '2px 8px 15px 1px #3c5b68',
      transition: '.5s'
    },
    '&:hover $priceDiv': {
      backgroundColor: '#FF0056',
      borderTopLeftRadius: 16,
      borderBottomLeftRadius: 16,
      color: 'white'
    }
  },
  image: {
    width: '100%',
    marginTop: 33,
    marginRight: 6,
    marginLeft: 6
  },
  topDiv: {
    position: 'relative'
  },
  statusDiv: {
    width: 79,
    height: 24,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 100,
    position: 'absolute',
    border: '1px solid #EC407A',
    bottom: 10,
    left: 8
  },
  title: {
    width: '100%',
    textAlign: 'center'
  },
  desc: {
    width: '100%',
    textAlign: 'center',
    marginTop: 2
  },
  bottomDiv: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  cartPlus: {
    width: 36,
    height: 31,
    marginBottom: 17,
    marginLeft: 16,
    cursor: 'pointer'
  },
  priceDiv: {
    width: 189,
    height: 32,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'right',
    alignItems: 'center',
    marginBottom: 19,
    paddingRight: 10,
    paddingBottom: 10
  },
  price: {
    marginRight: 10,
    color: theme.palette.textPrimary
  }
}));
