import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Categories from '../components/Categories';
import Products from '../components/Products';

export default function index() {
  return (
    <Switch>
      <Route exact path="/">
        <Categories />
      </Route>
      <Route exact path="/products">
        <Products />
      </Route>
    </Switch>
  );
}
