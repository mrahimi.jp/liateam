import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import useStyles from './MasterLayout.styles';
import DesktopMenu from '../components/Menus/desktop';
import Footer from '../components/Footer';
import Routes from '../routes';
import store from '../store';

export default function MasterLayout() {
  const classes = useStyles();

  return (
    <Provider store={store}>
      <BrowserRouter>
        <div className={classes.root}>
          <DesktopMenu />
          <Routes />
          <Footer />
        </div>
      </BrowserRouter>
    </Provider>
  );
}
