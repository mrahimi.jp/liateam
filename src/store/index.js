import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import cartReducer from '../reducers/cartReducers';

const storeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(cartReducer, storeEnhancers(applyMiddleware(thunk)));

export default store;
