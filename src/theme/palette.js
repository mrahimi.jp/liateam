export default {
  primary: {
    main: '#EC407A',
    contrastText: '#FFF'
  },
  secondary: {
    main: '#263943',
    contrastText: '#87e4b0'
  },
  text: {
    primary: '#202124',
    secondary: '#EC407A',
    dark: '#000',
    light: '#D8D8D8'
  },
  icon: {
    shopIcon: '#77a3bf'
  },
  border: {
    cartItemBorder: '#d7dadc',
    drawerItemBorder: '#393c41'
  }
};
