import { createTheme, responsiveFontSizes } from '@material-ui/core/styles';
import palette from './palette';

const theme = createTheme({
  direction: 'rtl',
  palette,
  spacing: 2,
  typography: {
    allVariants: {
      fontFamily: 'IRANSans'
    },
    h1: {
      fontSize: 35,
      fontWeight: 'bold'
    },
    h2: {
      fontSize: 36,
      color: palette.text.dark
    },
    h3: {
      fontSize: 24
    },
    h4: {
      fontSize: 23,
      fontWeight: 500
    },
    h5: {
      fontSize: 14
    },
    h6: {
      fontSize: 12
    },
    body1: {
      fontSize: 12,
      color: palette.text.primary
    },
    body2: {
      fontSize: 14
    },
    subtitle1: {
      fontSize: 15,
      color: palette.text.light
    },
    subtitle2: {
      fontSize: 12
    },
    caption: {
      fontSize: 12,
      fontWeight: 200
    },
    button: {
      fontSize: 15
    }
  }
});

export default responsiveFontSizes(theme);
